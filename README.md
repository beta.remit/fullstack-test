# BetaRemit - Fullstack Recruitment

An unattended fullstack recruitment test for BetaRemit. 

## The challenge
Using React Native + redux & Node.js, We want to implement single page application which can manage notes(in other words, memo).

And for managing notes, We need notebook which can contain notes.
Expected User behavior in organizing note will be like this: 

* Create note in the "Todo" notebook
* Move note in the "Todo" notebook to "Done" notebook
* Delete note in the "Done" notebook
* Sort notes in the "Todo" notebook via notebook title or update time

## Stack
1. React Native + Redux
2. Node js
4. MongoDb (mongoose)
5. Jest/Mocha

## Specifications
* The App can't be access without an account
* A user can only see his notes
* A note has title, content and update time.
* A notebook has title, description and set of notes.
* A note should be included in one notebook.
* On a note page, you can read/update/remove the note
* On a notebook page, you can manage notes - create, read, delete, move to other notebook, sort and filtering
* On notebook page, you should not show note content fully, you should show the title and few characters from its content
* On the Main page, you can see the notebooks and recent updated notes.
* All communication with backend side will be done through a REST API you will implement, just mock the API.
* 

## Requirements
The final product should:

* Be production ready
* Have good test coverage
* Have an elegant user experience and design
* Use appropriate front end tooling

## Deliverables
The project source code and dependencies should be made available in GitHub. Here are the steps you should follow:
1. Create a public repository on GitHub. It is always nice to see a clean commit history (create an account if you don't have one).
2. Create a "development" branch and commit the code to it. Do not push the code to the master branch.
3. Create a "screenshots" sub-folder and include at least two screenshots of the app.
4. Include a README file that describes:
  - Special build instructions, if any
  - List of third-party libraries used and short description of why/how they were used
5. Once the work is complete, create a pull request from "development" into "master" and send us the link.
6. Avoid using huge commits hiding your progress. Feel free to work on a branch and use rebase to adjust your commits before submitting the final version.

## Coding Standards
When working on the project be as clean and consistent as possible. Respect ES6 Rules

## Project Deadline
Ideally you'd finish the test project in 5 days. It shouldn't take you longer than an entire week.

## Quality Assurance
Use the following checklist to ensure high quality of the project.

## Evaluation Criteria
1. First of all, it should compile and run without errors
2. Understanding of design patterns, OO concepts and other related concepts.
3. The tools of choice to solve the problem.
4. Code and component reuse/extensibility.
5. Ability to write test code.
6. Code design.
7. Ability to write clear documentation.
8. Separation of frontend and backend.

## Submission
1. A link to the Github repository.
2. Briefly describe how you decided on the tools that you used.

## Bonus
* Any supplementary feature you wish to implement
* Integration tests

## Have Fun Coding 🤘
This challenge description is intentionally vague in some aspects, but if you need assistance feel free to ask for help.
